package Protocolo;

import java.io.*;
import java.net.*;

public class EchoClient {

    public static void main(String[] args) throws IOException {
        String host = "10.24.3.167";
//        String host = "localhost";
        int puerto =8888;
        String direcc, archivo, cont, cliente, contraseña, respuesta = "0";
        boolean login = false;
        try (
                Socket socket = new Socket(host, puerto);
                PrintWriter salida = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader entrada = new BufferedReader( new InputStreamReader(socket.getInputStream()));
                BufferedReader mensaje = new BufferedReader(new InputStreamReader(System.in))) {
            String usinput;
            System.out.println("Bienvenido Porfavor Escoja Una Opcion\n1:Inicio de sesion\n2:Registrarse");
            while ((usinput = mensaje.readLine()) != null) {
                if (usinput.equals("1") && login == false) {
                    while (respuesta.equals("0")) {
                        salida.println("Iniciando...");
                        System.out.println("Usuario: ");
                        cliente = mensaje.readLine();
                        System.out.println("Contraseña: ");
                        contraseña = mensaje.readLine();
                        usinput = cliente + "," + contraseña;
                        salida.println(usinput);
                        respuesta = entrada.readLine();
                        if (respuesta.equals("0")) {
                            System.out.println("Usuario/Contraseña incorrecta, Presione 1 para intentarlo de nuevo, Presione 2 para regresar al menu anterior");
                            respuesta = mensaje.readLine();
                            while (!respuesta.equals("1") && !respuesta.equals("2")) {
                                System.out.println("Opcion no valida, vuelva a intentarlo");
                                respuesta = mensaje.readLine();
                            }
                            if (respuesta.equals("1")) {
                                respuesta = "0";
                            } else if (respuesta.equals("2")) {
                                respuesta = "Cancelado";
                                System.out.println("Bienvenido Porfavor Escoja Una Opcion\n1:Inicio de sesion\n2:Registrarse");
                            }
                        } else if (respuesta.equals("1")) {
                            login = true;
                        }
                    }
                } else if (usinput.equals("2") && login == false) {
                    salida.println("Registrando...");
                    System.out.println("Nuevo usuario: ");
                    cliente = mensaje.readLine();
                    System.out.println("Nueva contraseña: ");
                    contraseña = mensaje.readLine();
                    usinput = cliente + "," + contraseña;
                    salida.println(usinput);
                    login = true;
                    System.out.println("Usuario creado");
                } else if (!usinput.equals("1") && !usinput.equals("2")) {
                    System.out.println("Opcion invalida");
                }
                if (login) {
                    break;
                }
                respuesta = "0";
                usinput = "";
            }
            respuesta = "0";
            System.out.println("Elija una opcion, presione A para listar archivos, presione B para mostrar detalles del archivo "+ "presione C para eliminar el archivo o presione D para salir");
            while ((usinput = mensaje.readLine()) != null) {
                if (usinput.equals("A") && login) {
                    respuesta = "0";
                    while (respuesta.equals("0")) {
                        salida.println("Listando...");
                        System.out.println("Indroduzca la ruta del directorio");
                        direcc = mensaje.readLine();
                        salida.println(direcc);
                        respuesta = entrada.readLine();
                        if (respuesta.equals("1")) {
                            cont = entrada.readLine();
                            String[] temp = cont.split(",");
                            System.out.println("Lista de archivos");
                            for (int i = 0; i < temp.length; i++) {
                                System.out.println(temp[i]);
                            }
                        } else if (respuesta.equals("0")) {
                            System.out.println("Ruta no valida");
                        }
                    }

                }
                if (usinput.equals("B") && login) {
                    respuesta = "0";
                    while (respuesta.equals("0")) {
                        salida.println("Revizando...");
                        System.out.println("Indroduzca la ruta del archivo");
                        direcc = mensaje.readLine();
                        salida.println(direcc);
                        respuesta = entrada.readLine();
                        if (respuesta.equals("1")) {
                            cont = entrada.readLine();
                            String temp[] = cont.split(",");
                            System.out.println("Detalles del archivo");
                            for (int i = 0; i < temp.length; i++) {
                                System.out.println(temp[i]);
                            }
                        } else if (respuesta.equals("0")) {
                            System.out.println("Ruta no valida");
                        }
                    }
                }
                if (usinput.equals("C") && login) {
                    respuesta = "0";
                    while (respuesta.equals("0")) {
                        salida.println("Eliminando");
                        System.out.println("Introduzca la ruta del archivo");
                        direcc = mensaje.readLine();
                        salida.println(direcc);
                        respuesta = entrada.readLine();
                        if (respuesta.equals("1")) {
                            System.out.println("Archivo eliminado");
                        } else if (respuesta.equals("0")) {
                            System.out.println("Ruta no valida");
                        }
                    }  
                }
                 if(usinput.equals("D")){
                        login = false;
                        socket.close();
                     System.exit(0);
                    }
                if (!usinput.equals("D")) {
                    System.out.println("Elija una opcion, presione A para listar archivos, presione B para mostrar detalles del archivo "+ "presione C para eliminar el archivo o presione D para salir");
                }
            }
        } catch (UnknownHostException e) {
            System.err.println("Host incorrecto " + host);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("No se ha podido conectar "+ host);
            System.exit(1);
        }
    }
}
