package Protocolo;

import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class EchoServerMulti {

    static ArrayList<String> Cliente = new ArrayList<String>();

    public static void main(String[] args) throws IOException {
        int puerto = 8888;
        try (
                ServerSocket socket2
                = new ServerSocket(puerto);) {

            while (true) {
                (new Hilo(socket2.accept())).start();
            }

        } catch (IOException e) {
            System.out.println("No se encontro el puerto" + puerto);
            System.out.println(e.getMessage());
        }
    }
}

class Hilo extends Thread {
    int est = 0;
    Socket socket1;

    public Hilo(Socket socket) {
        this.socket1 = socket;
    }

    @Override
    public void run() {
        try (
            PrintWriter output = new PrintWriter(socket1.getOutputStream(), true);
            BufferedReader input = new BufferedReader(new InputStreamReader(socket1.getInputStream()));) {
            System.out.println("Se acepto una conexion desde: " + socket1.getRemoteSocketAddress().toString());
            EchoServerMulti.Cliente.add("German,1234");
            String inputLine;
            while ((inputLine = input.readLine()) != null) {
                System.out.println(socket1.getRemoteSocketAddress().toString() + " - Leido : " + inputLine);
                if (inputLine.equals("Iniciando...")) {
                    String usu = input.readLine();
                    String[] datos = usu.split(",");
                    for (int i = 0; i < EchoServerMulti.Cliente.size(); i++) {
                        String tiempo[] = EchoServerMulti.Cliente.get(i).split(",");
                        if (datos[0].equals(tiempo[0]) && datos[1].equals(tiempo[1])) {
                            est = 1;
                        }
                    }
                    if (est == 1) {
                        output.println("1");
                    } else {
                        output.println("0");
                    }
                } else if (inputLine.equals("Registrando...")) {
                    String tiempo = input.readLine();
                    EchoServerMulti.Cliente.add(tiempo);
                } else if (inputLine.equals("Listando...")) {
                    String tiempo = input.readLine();
                    File archivo = new File(tiempo);
                    if (archivo.exists() && archivo.isDirectory()) {
                        output.println("1");
                        String r = this.Lista(archivo);
                        output.println(r);
                    } else {
                        output.println("0");
                    }
                } else if (inputLine.equals("Revizando...")) {
                    String temp = input.readLine();
                    File archivo = new File(temp);
                    if (archivo.exists() && archivo.isFile()) {
                        output.println("1");
                        String r = this.Leer(temp);
                        output.println(r);
                    } else {
                        output.println("0");
                    }
                } else if (inputLine.equals("Eliminado")) {
                    String temp = input.readLine();
                    File archivo = new File(temp);
                    if (archivo.exists() && archivo.isFile()) {
                        archivo.delete();
                        output.println("1");
                    } else {
                        output.println("0");
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("No se encontro el puerto");
            System.out.println(e.getMessage());
        }

    }

    public String Lista(final File carpeta) {
        String Li = "";
        for (final File ficheroEntrada : carpeta.listFiles()) {
            if (ficheroEntrada.isDirectory()) {
            } else {
                Li += ficheroEntrada.getName() + ",";
            }

        }
        return Li;
    }

    public String Leer(String f) {
        String contar = "";
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            archivo = new File(f);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);
            String linea;
            while ((linea = br.readLine()) != null) {
                contar = contar + linea + ",";
            }
        } catch (IOException ae) {
            ae.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (IOException ae) {
                ae.printStackTrace();
            }
        }
        return contar;
    }
}
